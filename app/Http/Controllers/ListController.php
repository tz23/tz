<?php

namespace App\Http\Controllers;
use App\Models\UserList;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ListController extends Controller
{
    
	public function __construct()
	{
	    $this->middleware('auth');
	}

    public function index() {
    	$id = Auth::id();

    	$list = UserList::where('user_id', $id)
               ->orderBy('name')
               ->get();

        return view('list.index', [
            'list' => $list
        ]);
    }

    public function edit($id) {
    	echo '<pre>'; 
    	print_r($id); 
    	echo '</pre>';
    	exit;
    }
}
